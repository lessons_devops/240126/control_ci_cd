#!/bin/bash
sudo apt update 
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt install -y nodejs
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install -y yarn

sudo apt install -y curl gcc g++ make nginx
sudo ufw allow 'Nginx HTTP' 

sudo cat << EOF > /etc/nginx/sites-available/default
server {
    listen  80;
    server_name localhost;
    location / {
        proxy_pass http://localhost:3000;
        proxy_http_version 1.1;
    }
}
EOF

sudo nginx -t

sudo service nginx restart


 sudo mkdir /home/vagrant/game && cd /home/vagrant/game
 sudo git clone https://gitlab.com/lessons_devops/240126/app-game.git
 cd  /home/vagrant/game/app-game/application/
 sudo npm install --include=dev
 sudo npm run build
 #nohup npx npm start &


sudo cat << EOF > /etc/systemd/system/node-webserver.service
 
[Unit]
Description=MyApp Service
After=network.target

[Service]
Type=simple
WorkingDirectory=/home/vagrant/game/app-game/application/
ExecStart=/usr/bin/npx npm start
Restart=always
User=vagrant
Group=vagrant
Environment=NODE_ENV=production

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl start node-webserver
 
sleep 10
 
 curl localhost:3000
 ss -lpnt

echo " node "
node -v
echo "npm  "
npm -v
echo "yarn  "
yarn -v
