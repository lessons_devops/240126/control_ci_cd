# Prod
   <img src="/vagrant/prod.png"  width="700" height="500">  
 
# Dev
   <img src="/vagrant/Dev.png"  width="700" height="500">    


Ansible roles :   
- [docker-install](https://gitlab.com/library585605/ansible_roles/docker-install.git)
- [docker_deploy_images](https://gitlab.com/library585605/ansible_roles/docker_deploy_images.git)
- [HAProxy](https://gitlab.com/library585605/ansible_roles/haproxy.git)   

Docker images:    
- [molecule_v24](https://gitlab.com/library585605/docker_images/molecule)   
- [terraform_for_yandex](https://gitlab.com/library585605/docker_images/terraform-ya) 
- [ansible](https://gitlab.com/library585605/docker_images/ansible) 

Gitlab includes:
- [ansible](https://gitlab.com/library585605/gitlab_includes/ansible.git) 
- [terraform](https://gitlab.com/library585605/gitlab_includes/terraform.git)
- [molecule_v24_test](https://gitlab.com/library585605/gitlab_includes/molecule_test) 
- [docker_build](https://gitlab.com/library585605/gitlab_includes/docker_build.git)


README

1. При запуске Vagrantfile, автоматическ разварачивается приложение с помощью Bash скрипта. Доступно по 80 порту.

2. Проект Сontrol_CI_CD

Указываем переменые в Gitlab:
```
SSH_PRIVATE_KEY - приватый ключ для ансибла.
TF_SECRETS_FILE - в этой переменной содержутся следующие переменые (для backend terraform):
	access_key=" "
	secret_key=" "
	dynamodb_table=" "
	dynamodb_endpoint = " "


TF_SECRETS_FILE_KEY - key.json для тераформа
TF_folder_id - folder_id в облаке яндекса.
```


# Создание инфраструктуры в Yandex Cloud для развертывания 

Для реализации данного Terraform сценария понадобится: активный платежный аккаунт в Yandex Cloud,    
достаточное количество средства для запуска  виртуальных машин, [Yandex CLI](https://cloud.yandex.ru/ru/docs/cli/quickstart), подготовленные SSH-ключи.

Для создания инфраструктуры в Yandex Cloud с помощью Terraform нужно:    
1. Подготовить облако к работе:    
    * [Зарегистрироваться](https://console.cloud.yandex.ru/) в Yandex Cloud.
    * [Подключить](https://cloud.yandex.com/ru/docs/billing/concepts/billing-account) платежный аккаунт. 
    * [Убедится](https://console.cloud.yandex.ru/billing) в наличии достаточного количества средств для создания девяти ВМ.
2. Установить и настроить Yandex Cloud CLI:
    * [Скачать](https://cloud.yandex.ru/ru/docs/cli/quickstart) Yandex Cloud CLI.
    * [Создать](https://cloud.yandex.ru/ru/docs/cli/quickstart#initialize) профиль 
3. [Создать](https://cloud.yandex.com/ru/docs/tutorials/infrastructure-management/terraform-quickstart#get-credentials) сервисный аккаунт с помощью CLI.

4. [Сгенерировать](https://cloud.yandex.ru/ru/docs/cli/operations/authentication/service-account#auth-as-sa) SA ключ в JSON формате для подключения Terraform к облаку с помощью CLI:    
`yc iam key create --service-account-name service-name -o key.json`   
Будет сгенерирован SA ключ, а в терминал будет выведена секретная информация   
    
Ниже пример для копировавние или создание файла:    
```
touch ./key.json && cat <<EOF > ./key.json
 {
   "id": "",
   "service_account_id": "",
   "created_at": "2024-02-29T13:59:04.302546674Z",
   "key_algorithm": "RSA_2048",
   "public_key": "",
   "private_key": ""
   } 
EOF
```

5. Создать профель    
```
yc config profile create sa-profile && \
yc config set service-account-key /root/key.json && \
yc config set cloud-id  ххххххх && \
yc config set folder-id хххххх
```

6. Добавить в файл `~/.terraformrc` (если файла нет – создать его) следующий блок:
    ```
    provider_installation {
        network_mirror {
            url = "https://terraform-mirror.yandexcloud.net/"
            include = ["registry.terraform.io/*/*"]
        }
        direct {
            exclude = ["registry.terraform.io/*/*"]
        }
    }
    ```
7. [Настроить](https://cloud.yandex.com/ru/docs/tutorials/infrastructure-management/terraform-quickstart#configure-provider) Yandex Cloud Terraform провайдера.
8. Скачать данный репозиторий командой `git clone https://  `.
9. Перейти в директорию `yandex_cloud` (директория в скаченном репозитории) и внести изменения в следующие переменные, в файле `variables.tf`:
    * `key_path` – путь к сгенерированному SA ключу с помощью CLI.
    * `cloud_id` – ID облака. Можно получить список доступных облаков командой `yc resource-manager cloud list`.
    * `folder_id` – ID Cloud folder. Можно получить командой `yc resource-manager folder list`.

Теперь, находясь в директории `terraform`, можно выполните команду `terraform init` для установки провайдера и инициализации модулей. Далее нужно выполнить серию команд:
* `terraform plan` – создание плана будущей инфраструктуры.
* `terraform init` (повторный вызов) – создание ресурсов в облаке.

## Значения переменных 

Глобальные переменные проекта, содержащие данные для аутентификации, конфигурации виртуальных серверов,  находятся в корневом `variables.tf`, а локальные переменные модулей находятся в `variables.tf`, в каждом модуле. Переменные корневого файла `variables.tf`:

| Название переменной | Описание | Тип | Дефолтное значение | Примечание |
|---------------------|----------|-----|--------------------| ---------- |
| `key_path` | Путь до SA ключа в JSON формате. | string | "./key.json" | Создать SA ключ можно с помощью CLI командой `yc iam key create \ --service-account-id < service acc ID > \ --folder-name <folder cloud name> \ --output key.json`. |
| `cloud_id` | ID облака. | string | `<yandex cloud ID >` | Можно получить список доступных облаков командой `yc resource-manager cloud list`.|
| `profile` |  | string | "sa-profile" |   |
| `folder_id` | ID папки в Yandex Cloud. | string | `<yandex cloud folder ID>` | ID папки Yandex Cloud можно получить командой `yc resource-manager folder list`. |
| `vm_count` | Количество ВМ, которое будет создано в облаке. | number | 2 | Минимальное количество ВМ для создания  – 2 ВМ. |
| `static_node_disk_per_vm` | Количество подключаемых дисков к ВМ. |  | |
| `vps_platform` | Тип платформы виртуализации. | string | `standard-v3` | standard-v1 – Intel Broadwell, -v2 – Intel Cascade Lake, -v3 – Intel Ice Lake. |
| `static_node_attached_disk_name` | Название подключаемого к ВМ диска. | string | "data-disk" | |
| `user` | Пользователь для подключения по SSH. | string | "ubuntu" | |
| `ssh_key_pub_path` | Путь к публичной части SSH-ключа. | string | "<path to SSH pub>" | Создать пару SSH-ключей можно командой `ssh keygen`.|
| `zone_name` | Зоны доступности Yandex Cloud. | list | ["ru-central1-a", "ru-central1-b", "ru-central1-d"] | Список зон доступности не содержит ru-central1-c зоны – она выводится из эксплуатации. | 
| `domain` | Имя доменной DNS зоны. | string | "test.ru." | Имя доменной зоны всегда должно заканчиваться точкой. |   



Скриншоты:

Тест HAproxy:

   <img src="/vagrant/HAproxy.png"  width="500" height="500">    


