output "instances_fqdn" {
    description = "List of YDB nodes hostnames."
    value = yandex_compute_instance.vm.*.fqdn
}

output "instances_ips" {
  description = "List of YDB nodes external IPs."
  value = yandex_compute_instance.vm.*.network_interface.0.nat_ip_address
}

output "instances_ip" {
  description = "List of YDB nodes external IPs."
  value = yandex_compute_instance.vm.*.network_interface.0.ip_address
}  

 
 