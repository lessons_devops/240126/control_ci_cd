#============== Create VM for static nodes ================#

resource "yandex_compute_instance" "vm" {
  count = var.instance_count
  platform_id = var.instance_platform
  name  = "${var.instance_name}-${1 + count.index}"
  zone  = element(var.auth_zone_name, count.index % length(var.auth_zone_name))
  allow_stopping_for_update = true
  hostname = "${var.instance_hostname}-${1 + count.index}.${var.module_domain}"
  
  #element - выбирает элемент из списка
  #zone
      # 0 % 3 = 0
      # 1 % 3 = 1
      # 2 % 3 = 2
      # 3 % 3 = 0
      # 4 % 3 = 1
      # 5 % 3 = 2

  resources {
    cores  = var.instance_cores
    memory = var.instance_memory
    core_fraction = 20	
  }

  boot_disk {
    initialize_params {
      image_id = var.instance_image_id
      size = var.boot_disk_size
      type = var.instance_boot_disk_type
      name = "${var.instance_name}-${1 + count.index}-boot"
      # instance-0-boot
      # instance-1-boot
      # instance-2-boot
    }
  }

#   dynamic "secondary_disk" 


  network_interface {
    subnet_id = var.input_subnet_ids[0]
    nat       = true
    # subnet_id = element(var.input_subnet_ids, count.index % length(var.input_subnet_ids))
    # Используется остаток от деления индекса на длину списка подсетей. 
    # Это позволяет равномерно распределять экземпляры по разным подсетям в случае, если подсетей несколько.
    #nat       = count.index == 0 ? true : false
   
  }

  metadata = {
    ssh-keys = "var.module_user:${file(var.module_ssh_key_pub_path)}"
  }

}
