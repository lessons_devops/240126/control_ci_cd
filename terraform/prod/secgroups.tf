resource "yandex_vpc_security_group" "secgroup_k8s" {
  name        = "secgroup-k8s"
  network_id  = yandex_vpc_network.default.id
# allow loadbalancer healthchecks
  ingress {
    protocol          = "TCP"
    predefined_target = "loadbalancer_healthchecks"
    from_port         = var.application_port
    to_port           = var.application_port
  }
# allow internal traffic
  ingress {
    description       = "Allow traffic for Master-Node and Node-Node"
    protocol          = "ANY"
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }
  ingress {
    description    = "Allow traffic for Pod-Pod and Service-Service"
    protocol       = "ANY"
    v4_cidr_blocks = ["10.128.0.0/24"]
    from_port      = 0
    to_port        = 65535
    }
# allow whitelist traffic
  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = var.whitelist_ip
    from_port      = var.application_port
    to_port        = var.application_port
  }

# allow all outgoing traffic
  egress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}
