variable "cloud_id" {
  description = "Cloud ID"
  type        = string
  default     = ""  # удалить
}
variable "folder_id" {
  description = "Folder ID"
  type        = string
  default     = ""  # удалить
}
variable "zone" {
  description = "Zone"
  type        = string
  default     = "ru-central1-d"
} 

variable "master_version" {
  type        = string
  description = "Kubernetes version for master nodes"
  default     = "1.28"
}

variable "nodes_version" {
  type        = string
  description = "Kubernetes version for nodes"
  default     = "1.28"
}
variable "node_groups_defaults" {
  description = "Map of common default values for Node groups."
  type        = map(any)
  default     = {
    platform_id   = "standard-v3"
    node_cores    = 2
    node_memory   = 2
    node_gpus     = 0
    core_fraction = 20
    disk_type     = "network-ssd"
    disk_size     = 32
    preemptible   = false  //  прерываемые
    nat           = false
    ipv4          = true
    ipv6          = false
    size          = 1
  }
}

# security groups
variable "application_port" {
  description = "app port"
  type = number
  default = 80
}

variable "whitelist_ip" {
  description = "ip addresses for which the application can be accessed"
  type = list(string)
  default = ["0.0.0.0/0"]
}


